﻿using UnityEngine;
using System.Collections;

namespace MagicalFX
{
	public class FX_HitSpawner : MonoBehaviour
	{
		public GameObject FXSpawn;
		public bool DestoyOnHit = false;
		public bool FixRotation = false;
		public float LifeTimeAfterHit = 1;
		public float LifeTime = 0;
		public AudioClip bombSound;
		public AudioSource audioSource;
		[SerializeField]
		private CameraShake cameraShake;

		void Start ()
		{
			audioSource = GetComponent<AudioSource>();
			//Shake 스크립트를 추출
			cameraShake = GameObject.Find("Camera").GetComponent<CameraShake>();
		}
	
		void Spawn ()
		{
			if (FXSpawn != null) {
				Quaternion rotate = this.transform.rotation;
				if (!FixRotation)
					rotate = FXSpawn.transform.rotation;
				GameObject fx = (GameObject)GameObject.Instantiate (FXSpawn, this.transform.position, rotate);
				StartCoroutine(cameraShake.ShakeCamera(0.1f, 0.2f, 0.5f));
				audioSource.PlayOneShot(bombSound);
				if (LifeTime > 0)
					GameObject.Destroy (fx.gameObject, LifeTime);
			}
			if (DestoyOnHit) {
				GameObject.Destroy (this.gameObject, LifeTimeAfterHit);
				if (this.gameObject.GetComponent<Collider>())
					this.gameObject.GetComponent<Collider>().enabled = false;
			}
		}
	
		void OnTriggerEnter (Collider other)
		{
			Spawn ();
		}
	
		void OnCollisionEnter (Collision collision)
		{
			Spawn ();
		}
	}
}