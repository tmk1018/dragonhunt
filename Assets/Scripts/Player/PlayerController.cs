﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public enum PlayerState
    {
        Movement,
        Attack,
        Dodge,
        Damage,
        Skill1,
        Skill2,
        Skill3,
        Dead
    }


    private PlayerStateMachine<PlayerController> p_sm;
    //스테이트들을 보관할 딕셔너리 설정
    private Dictionary<PlayerState, IState<PlayerController>> p_states = new Dictionary<PlayerState, IState<PlayerController>>();
    //참조용 스크립트 선언
    public PlayerMovement playerMovement;

    void Start()
    {
        p_states.Add(PlayerState.Movement, new Movement());
        p_states.Add(PlayerState.Attack, new Attack());
        p_states.Add(PlayerState.Dodge, new Dodge());
        p_states.Add(PlayerState.Skill1, new Skill1());
        p_states.Add(PlayerState.Skill2, new Skill2());
        p_states.Add(PlayerState.Skill3, new Skill3());
        p_states.Add(PlayerState.Dead, new Dead());

        p_sm = new PlayerStateMachine<PlayerController>(this, p_states[PlayerState.Movement]);

        //스크립트 연결
        playerMovement = GetComponent<PlayerMovement>();
    }


    public void ChangeState(PlayerState state)
    {
        Debug.LogWarning(state);
        p_sm.SetState(p_states[state]);
    }


    private void Update()
    {
        p_sm.OnUpdate();
    }

    private void FixedUpdate()
    {
        p_sm.OnFixedUpdate();
    }


}
