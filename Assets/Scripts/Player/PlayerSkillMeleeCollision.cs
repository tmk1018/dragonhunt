﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkillMeleeCollision : MonoBehaviour
{
    [SerializeField]
    private float skillDmg;
    [SerializeField]
    private float remainTime;

    private void OnEnable()
    {
        StartCoroutine("AutoDisable");
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Boss"))
        {
            other.GetComponent<EnemyTakeDamage>().ETakeDamageWithSound(skillDmg);
            other.GetComponent<EnemyTakeDamage>().CameraShake();
            this.gameObject.SetActive(false);
        }
    }


    private IEnumerator AutoDisable()
    {
        yield return new WaitForSeconds(remainTime);

        gameObject.SetActive(false);
    }

}
