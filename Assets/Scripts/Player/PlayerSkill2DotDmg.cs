﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkill2DotDmg : MonoBehaviour
{
    [SerializeField] private float dotDmg;
    private float currentDamageTime;

    [SerializeField] private float remainTime;
    [SerializeField] private float damageTime;

    private void Start()
    {
        StartCoroutine("AutoDisable");
    }

    private void Update()
    {
        ElapseTime();
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Boss"))
        {
            if (currentDamageTime <= 0)
            {
                other.GetComponent<EnemyTakeDamage>().ETakeDamage(dotDmg);
                currentDamageTime = damageTime;
            }
        }
    }
    private IEnumerator AutoDisable()
    {
        yield return new WaitForSeconds(remainTime);
        Destroy(this.gameObject);
    }
    private void ElapseTime()
    {
        if (currentDamageTime > 0)
            currentDamageTime -= Time.deltaTime;  // 1초에 1씩
    }
}
