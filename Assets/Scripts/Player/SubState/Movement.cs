﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Movement : IState<PlayerController>
{
    public void OnEnter(PlayerController player)
    {
    }

    public void OnExit(PlayerController player)
    {
    }

    public void OnFixedUpdate(PlayerController player)
    {
        player.playerMovement.OnMovement();
    }

    public void OnUpdate(PlayerController player)
    {

        if (player.playerMovement.input_attackbtn)
            player.ChangeState(PlayerController.PlayerState.Attack);
        if (player.playerMovement.input_dodgebtn)
            player.ChangeState(PlayerController.PlayerState.Dodge);
        if (player.playerMovement.input_skill1btn)
            player.ChangeState(PlayerController.PlayerState.Skill1);
        if (player.playerMovement.input_skill2btn)
            player.ChangeState(PlayerController.PlayerState.Skill2);
        if (player.playerMovement.input_skill3btn)
            player.ChangeState(PlayerController.PlayerState.Skill3);
    }


}
