﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Dodge : IState<PlayerController>
{
    public void OnEnter(PlayerController player)
    {
        player.playerMovement.weaponTrailOff();
        player.playerMovement.AttackDelayOff();
        player.playerMovement.animator.rootPosition = Vector3.zero;
        player.playerMovement.animator.SetTrigger("Dodge");
    }

    public void OnExit(PlayerController player)
    {
        player.playerMovement.animator.rootPosition = Vector3.zero;
        player.playerMovement.AnimReset();
    }

    public void OnFixedUpdate(PlayerController player)
    {

    }

    public void OnUpdate(PlayerController player)
    {
        if (player.playerMovement.animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.8f)
            player.ChangeState(PlayerController.PlayerState.Movement);


    }


}
