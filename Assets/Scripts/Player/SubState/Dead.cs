﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Dead : IState<PlayerController>
{
    public void OnEnter(PlayerController player)
    {
        player.playerMovement.animator.SetTrigger("Dead");
    }

    public void OnExit(PlayerController player)
    {
    }

    public void OnFixedUpdate(PlayerController player)
    {

    }

    public void OnUpdate(PlayerController player)
    {
    }
}
