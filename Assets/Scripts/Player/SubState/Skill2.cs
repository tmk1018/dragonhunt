﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Skill2 : IState<PlayerController>
{
    public void OnEnter(PlayerController player)
    {
        player.playerMovement.weaponTrailOff();
        player.playerMovement.AttackDelayOff();
        player.playerMovement.animator.SetTrigger("Skill_2");
    }

    public void OnExit(PlayerController player)
    {
    }

    public void OnFixedUpdate(PlayerController player)
    {
    }

    public void OnUpdate(PlayerController player)
    {
        if (player.playerMovement.animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.8f)
            player.ChangeState(PlayerController.PlayerState.Movement);


    }


}
