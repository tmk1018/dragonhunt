﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Attack : IState<PlayerController>
{
    public void OnEnter(PlayerController player)
    {
        player.playerMovement.OnWeaponAttack();
    }

    public void OnExit(PlayerController player)
    {
    }

    public void OnFixedUpdate(PlayerController player)
    {

    }

    public void OnUpdate(PlayerController player)
    {
        if (player.playerMovement.input_dodgebtn)
        {
            player.ChangeState(PlayerController.PlayerState.Dodge);
        }
        else if (player.playerMovement.input_attackbtn &&
            player.playerMovement.animator.GetCurrentAnimatorStateInfo(0).normalizedTime <= 0.5f)
        {
            return;
        }
        else
            player.ChangeState(PlayerController.PlayerState.Movement);
    }
}
