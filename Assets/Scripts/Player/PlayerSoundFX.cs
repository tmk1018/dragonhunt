﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSoundFX : MonoBehaviour
{
    [SerializeField] public AudioClip[] clips;
    public AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }


    public void MeleeAttack1Sound()
    {
        audioSource.PlayOneShot(clips[0]);
    }

    public void MeleeAttack2Sound()
    {
        audioSource.PlayOneShot(clips[1]);
    }

    public void MeleeAttack3Sound()
    {
        audioSource.PlayOneShot(clips[2]);
    }

    public void Skill1_1Sound()
    {
        audioSource.PlayOneShot(clips[3]);
    }
    
    public void Skill1_2Sound()
    {
        audioSource.PlayOneShot(clips[4]);
    }

    public void Skill1_3Sound()
    {
        audioSource.PlayOneShot(clips[5]);
    }

    public void Skill2_1Sound()
    {
        audioSource.PlayOneShot(clips[6]);
    }
    
    public void Skill2_2Sound()
    {
        audioSource.PlayOneShot(clips[7]);
    }

    public void Skill2_3Sound()
    {
        audioSource.PlayOneShot(clips[8]);
    }

    public void Skill2_4Sound()
    {
        audioSource.PlayOneShot(clips[9]);
    }

    public void Skill3_1Sound()
    {
        audioSource.PlayOneShot(clips[10]);
    }

    public void Skill3_2Sound()
    {
        audioSource.PlayOneShot(clips[11]);
    }

    public void PlayerDeadSound()
    {
        audioSource.PlayOneShot(clips[12]);
    }

    public void PlayerDodgeSound()
    {
        audioSource.PlayOneShot(clips[13]);
    }
}
