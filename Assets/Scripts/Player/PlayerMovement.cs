﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XftWeapon;

public class PlayerMovement : MonoBehaviour
{
    public float input_horizontal;
    public float input_vertical;

    public bool input_attackbtn;
    public bool input_dodgebtn;
    public bool input_skill1btn;
    public bool input_skill2btn;
    public bool input_skill3btn;

    public Animator animator;

    public XWeaponTrail ProTrailDistort;
    public XWeaponTrail ProTrailShort;
    public XWeaponTrail ProTraillong;

    public bool attackDelay; // 공격시 한번만 데미지가 들어가게끔

    public GameObject skill1Collision;
    public GameObject skill2_1Collision;
    public GameObject skill2_2Collision;
    public GameObject skill2_3Collision;
    public GameObject skill2_4Collision;
    public GameObject skill2_5Collision;
    public GameObject skill2_6Collision;
    public GameObject skill3_SlashCollision;
    public GameObject skill3_PhoenixCollision;

    public Transform skill2_6CollisionPosition;

    public PlayerStepSoundFX playerStepSoundFX;

    private CameraShake cameraShake;
    void Start()
    {
        Cursorvis();
        animator = GetComponent<Animator>();
        cameraShake = GetComponentInChildren<CameraShake>();
    }

    // Update is called once per frame
    void Update()
    {
        KeyboardInput();
    }

    //키보드인풋
    void KeyboardInput() //키보드 입력 관련
    {
        input_horizontal = Input.GetAxis("Horizontal");
        input_vertical = Input.GetAxis("Vertical");

        input_attackbtn = Input.GetMouseButtonUp(0);
        input_dodgebtn = Input.GetKeyDown(KeyCode.Space);
        input_skill1btn = Input.GetKeyDown(KeyCode.Alpha1);
        input_skill2btn = Input.GetKeyDown(KeyCode.Alpha2);
        input_skill3btn = Input.GetKeyDown(KeyCode.Alpha3);
    }

    
    public void OnMovement() //이동 관련
    {
        float horizontalOffset = 1.0f + Input.GetAxis("Sprint") * 1.0f;
        float verticalOffset = 1.0f + Input.GetAxis("Sprint") * 1.0f;

        Mathf.Clamp(horizontalOffset, 0, 2);
        Mathf.Clamp(verticalOffset, 0, 2);
        animator.SetFloat("Horizontal", input_horizontal * horizontalOffset);
        animator.SetFloat("Vertical", input_vertical * verticalOffset);
    }

    public void OnWeaponAttack() //공격 애니메이션 트리거 실행
    {
        animator.SetTrigger("Attack");
    }

    public void AnimReset() //실행중인 애니메이션 초기화
    {
        animator.SetFloat("Horizontal", 0f);
        animator.SetFloat("Vertical", 0f);

    } 

public void Cursorvis()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void weaponTrailOn()
    {
        ProTrailDistort.Activate();
        ProTrailShort.Activate();
        ProTraillong.Activate();
    }
        
    public void weaponTrailOff()
    {
        ProTrailDistort.StopSmoothly(0.3f);
        ProTrailShort.StopSmoothly(0.3f);
        ProTraillong.StopSmoothly(0.3f);
    }

    public void AttackDelayOn()
    {
        attackDelay = true;
    }

    public void AttackDelayOff()
    {
        attackDelay = false;
    }

    public void Skill1CollisionOn()
    {
        skill1Collision.SetActive(true);
    }

    public void Skill2_1CollisionOn()
    {
        skill2_1Collision.SetActive(true);
    }
    public void Skill2_2CollisionOn()
    {
        skill2_2Collision.SetActive(true);
    }
    public void Skill2_3CollisionOn()
    {
        skill2_3Collision.SetActive(true);
    }
    public void Skill2_4CollisionOn()
    {
        skill2_4Collision.SetActive(true);
    }
    public void Skill2_5CollisionOn()
    {
        skill2_5Collision.SetActive(true);
    }
    public void Skill2_6CollisionOn()
    {
        Instantiate(skill2_6Collision, skill2_6CollisionPosition.position, skill2_6CollisionPosition.rotation);
    }
    public void Skill3_SlashCollisionOn()
    {
        skill3_SlashCollision.SetActive(true);
    }
    public void Skill3_PhoenixCollisionOn()
    {
        skill3_PhoenixCollision.SetActive(true);
    }

    public void CameraShakeForSkill1()
    {
        StartCoroutine(cameraShake.ShakeCamera(0.1f, 0.2f, 0.5f));
    }
}
