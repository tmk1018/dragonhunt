﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStepSoundFX : MonoBehaviour
{
    [SerializeField] public AudioClip[] clips;
    public AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }
    public void LeftStepSound()
    {
        if(!audioSource.isPlaying)
            audioSource.PlayOneShot(clips[0]);
    }
    public void RightStepSound()
    {
        if (!audioSource.isPlaying)
            audioSource.PlayOneShot(clips[1]);
    }
}
