﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTakeDamage : MonoBehaviour
{
    [SerializeField]
    private float hp = 100;

    private PlayerController playerController;
    private PlayerSoundFX playerSoundFX;
    private void Start()
    {
        playerController = GetComponent<PlayerController>();
        playerSoundFX = GetComponent<PlayerSoundFX>();
    }

    private void Update()
    {
        PlayerDead();
    }

    public void PTakeDamage(float damage)
    {
        hp -= damage;
        Debug.Log("PTakeDamage");
    }
    public void PTakeDamageWithSound(float damage)
    {
        int ranInt = Random.Range(14, 15);
        hp -= damage;
        playerSoundFX.audioSource.PlayOneShot(playerSoundFX.clips[ranInt]);
    }
    void PlayerDead()
    {
        if (hp <= 0)
        {
            playerController.ChangeState(PlayerController.PlayerState.Dead);
        }
    }
}
