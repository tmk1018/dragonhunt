﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkillLongRangeCollision : MonoBehaviour
{
    [SerializeField]
    private Transform startCollisionPoint;
    [SerializeField]
    private Transform endCollisionPoint;
    [SerializeField]
    private float speed;
    private float distance;
    [SerializeField]
    private float skillDmg;
    private void OnEnable()
    {
        this.transform.position = startCollisionPoint.position;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Boss"))
        {
            other.GetComponent<EnemyTakeDamage>().ETakeDamage(skillDmg);
            this.gameObject.SetActive(false);
        }
    }

    private void Update()
    {
        transform.position = Vector3.MoveTowards(this.transform.position, 
                                endCollisionPoint.position, speed * Time.deltaTime);
        distance = Vector3.Distance(this.transform.position, endCollisionPoint.position);
        if (distance <= 1f)
            this.gameObject.SetActive(false);
    }


}
