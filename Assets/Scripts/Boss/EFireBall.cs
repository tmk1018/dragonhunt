﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EFireBall : MonoBehaviour
{
    [SerializeField]
    private float fireBallDmg;
    [SerializeField]
    private AudioClip bombSound;
    private AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerTakeDamage>().PTakeDamageWithSound(fireBallDmg);
            audioSource.PlayOneShot(bombSound);
            Destroy(this);
        }
    }
}
