﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPattern : MonoBehaviour
{
    [SerializeField]
    private EnemyController enemyController;
    public int enemyPatternCnt = 5;
    public IEnumerator patternA()
    {   
        Debug.Log("patternA");
        enemyController.ChangeState(EnemyController.EnemyState.ETargetingPlayer);
        yield return new WaitForSeconds(1.5f);
        enemyController.ChangeState(EnemyController.EnemyState.EMeleeAttackLeft);
        yield return new WaitForSeconds(1.667f);
        enemyController.ChangeState(EnemyController.EnemyState.EMeleeAttackRight);
        yield return new WaitForSeconds(1.667f);
        enemyController.ChangeState(EnemyController.EnemyState.EIdle);
        yield break;
    }

    public IEnumerator patternB()
    {
        Debug.Log("patternB");
        enemyController.ChangeState(EnemyController.EnemyState.ETargetingPlayer);
        yield return new WaitForSeconds(1.5f);
        enemyController.ChangeState(EnemyController.EnemyState.EMeleeAttackRight);
        yield return new WaitForSeconds(1.667f);
        enemyController.ChangeState(EnemyController.EnemyState.EMeleeAttackLeft);
        yield return new WaitForSeconds(1.667f);
        enemyController.ChangeState(EnemyController.EnemyState.EMeleeAttackRight);
        yield return new WaitForSeconds(1.667f);
        enemyController.ChangeState(EnemyController.EnemyState.EIdle);
        yield break;
    }

    public IEnumerator patternC()
    {
        Debug.Log("patternC");
        enemyController.ChangeState(EnemyController.EnemyState.ETargetingPlayer);
        yield return new WaitForSeconds(1.5f);
        enemyController.ChangeState(EnemyController.EnemyState.EMeleeAttackLeft);
        yield return new WaitForSeconds(1.667f);
        enemyController.ChangeState(EnemyController.EnemyState.EMeleeAttackRight);
        yield return new WaitForSeconds(1.667f);
        enemyController.ChangeState(EnemyController.EnemyState.ETargetingPlayer);
        yield return new WaitForSeconds(1f);
        enemyController.ChangeState(EnemyController.EnemyState.EFireBreath);
        yield return new WaitForSeconds(3.333f);
        enemyController.ChangeState(EnemyController.EnemyState.EIdle);
        yield break;
    }

    public IEnumerator patternD()
    {
        Debug.Log("patternD");
        enemyController.ChangeState(EnemyController.EnemyState.ETargetingPlayer);
        yield return new WaitForSeconds(1.5f);
        enemyController.ChangeState(EnemyController.EnemyState.EFireBreath);
        yield return new WaitForSeconds(3.333f);
        enemyController.ChangeState(EnemyController.EnemyState.EIdle);
        yield break;
    }

    public IEnumerator patternE()
    {
        Debug.Log("patternE");
        enemyController.ChangeState(EnemyController.EnemyState.ETargetingPlayer);
        yield return new WaitForSeconds(1.5f);
        enemyController.ChangeState(EnemyController.EnemyState.ESpitFire);
        yield return new WaitForSeconds(1f);
        enemyController.ChangeState(EnemyController.EnemyState.EIdle);
        yield break;
    }

    public IEnumerator patternUlt()
    {
        Debug.Log("patternUlt");
        enemyController.ChangeState(EnemyController.EnemyState.EFlying);
        yield return new WaitForSeconds(5f);
        enemyController.ChangeState(EnemyController.EnemyState.EUlt);
        yield return new WaitForSeconds(10f);
        enemyController.ChangeState(EnemyController.EnemyState.ELanding);
        yield break;

    }

    public IEnumerator WaitSec(float sec)
    {
        yield return new WaitForSeconds(sec);
    }

    public IEnumerator stopCoroutine()
    {
        StopAllCoroutines();
        yield break;
    }
}
