﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySoundFX : MonoBehaviour
{
    [SerializeField] public AudioClip[] clips;
    public AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }
    public void LeftMeleeAttackSound()
    {
        audioSource.PlayOneShot(clips[0]);
    }
    public void RightMeleeAttackSound()
    {
        audioSource.PlayOneShot(clips[1]);
    }
    public void FireBreathSound_1()
    {
        audioSource.PlayOneShot(clips[2]);
    }
    public void FireBreathSound_2()
    {
        audioSource.PlayOneShot(clips[3]);
    }
    public void MeleeAttackSound()
    {
        audioSource.PlayOneShot(clips[4]);
    }
    public void DeadSound()
    {
        audioSource.PlayOneShot(clips[11]);
    }
    public void DragonWingSound()
    {
        audioSource.PlayOneShot(clips[14]);
    }

    public void EUltSound()
    {
        audioSource.PlayOneShot(clips[15]);
    }
}
