﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehavior : MonoBehaviour
{
    [SerializeField]
    public float distanceFromPlayer;
    public float attackDistance;
    public float sightDistance;
    public float attackTurnSpeed;

    public GameObject player;
    public GameObject enemy;
    public GameObject breathStart; //브레스 시작지점
    public GameObject EBreath; //브레스 파티클
    public GameObject EFireBall;
    public GameObject leftMeleeAttackTrail;
    public GameObject rightMeleeAttackTrail;
    public Transform ultPosition;
    public GameObject eMateorObject;
    public Transform ultEndPosition;

    public GameObject leftAttackCollision;
    public GameObject rightAttackCollision;

    public GameObject eUltEffect;
   
    public float turnSpeed;

    [SerializeField]
    public EnemySoundFX enemySoundFX;

    [SerializeField]
    public GameObject enemyBreathCollisionSpawner;

    [SerializeField]
    public bool deadCheck = false;

    [SerializeField]
    public EnemyTakeDamage enemyTakeDamage;
    void Start()
    {
        enemy = this.gameObject;
        enemySoundFX = GetComponent<EnemySoundFX>();
    }
    void Update()
    {
        DistUpdate(); //플레이어와 에너미의 거리 업데이트
    }

    public void LookAtPlayerForTracking() //트래킹시 플레이어방향으로 회전
    {
        Vector3 dir = (player.transform.position - enemy.transform.position);
        this.transform.rotation = Quaternion.Lerp(this.transform.rotation,
            Quaternion.LookRotation(dir), Time.deltaTime * turnSpeed);
    }
    public void LookAtPlayerForAttack() //공격전 플레이어방향으로 회전
    {
        Vector3 dir = (player.transform.position - enemy.transform.position);
        this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(dir), Time.deltaTime * attackTurnSpeed);
    }

    private void DistUpdate() //플레이어와 에너미의 거리 업데이트
    {
        distanceFromPlayer = (player.transform.position - enemy.transform.position).sqrMagnitude;
    }

    public void FireBreath()
    {
        Vector3 dir = (player.transform.position - breathStart.transform.position).normalized;
        GameObject fireBreath = Instantiate(EBreath, breathStart.transform.position, breathStart.transform.rotation);
        fireBreath.transform.parent = breathStart.transform;
        Destroy(fireBreath, 8f);
    }

    public void SpitFire()
    {
        Vector3 dir = (player.transform.position - breathStart.transform.position).normalized;
        GameObject fireBall = Instantiate(EFireBall, breathStart.transform.position,
                                          breathStart.transform.rotation);
    }

    [System.Obsolete]
    public void leftMeleeTrailOn()
    {
        leftMeleeAttackTrail.SetActiveRecursively(true);
    }

    [System.Obsolete]
    public void leftMeleeTrailOff()
    {
        leftMeleeAttackTrail.SetActiveRecursively(false);
    }

    [System.Obsolete]
    public void rightMeleeTrailOn()
    {
        rightMeleeAttackTrail.SetActiveRecursively(true);
    }

    [System.Obsolete]
    public void rightMeleeTrailOff()
    {
        rightMeleeAttackTrail.SetActiveRecursively(false);
    }

    public void OnLeftAttackCollision()
    {
        leftAttackCollision.SetActive(true);
    }
    public void OnRightAttackCollision()
    {
        rightAttackCollision.SetActive(true);
    }

    public void OnEnemyBreathCollisionSpawner()
    {
        enemyBreathCollisionSpawner.SetActive(true);
    }
    public void OffEnemyBreathCollisionSpawner()
    {
        enemyBreathCollisionSpawner.SetActive(false);
    }
    public void EFlyingToUltpos()
    {
        this.transform.position = Vector3.MoveTowards(this.transform.position, ultPosition.transform.position, 0.07f);
    }
    public void EMateorOn()
    {
        eMateorObject.SetActive(true);
    }
    public void EMateorEnd()
    {
        eMateorObject.SetActive(false);
    }
    public void EFlyingToLand()
    {
        this.transform.position = Vector3.MoveTowards(this.transform.position, ultEndPosition.transform.position, 0.07f);
    }
    public bool ELandingCheck()
    {
        float distanceFromLand = (this.transform.position - ultEndPosition.transform.position).sqrMagnitude;

        if (distanceFromLand <= 0.1f)
        {
            return true;
        }
        else
            return false;
    }
    public void EUltEffect()
    {
        GameObject ulteffect = Instantiate(eUltEffect, this.transform.position, Quaternion.Euler(90, 0, 0));
    }
}
