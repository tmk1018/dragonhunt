﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLeftAttackCollision : MonoBehaviour
{
    [SerializeField]
    private ObjectPoolManager objectPoolManager;
    [SerializeField]
    private CameraShake cameraShake;
    private void OnEnable()
    {
        StartCoroutine("AutoDisable");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerTakeDamage>().PTakeDamageWithSound(10);
            StartCoroutine(cameraShake.ShakeCamera(0.07f, 0.1f, 0.25f));
            ShowBloodEffect(other);
        }
    }

    private IEnumerator AutoDisable()
    {
        yield return new WaitForSeconds(0.1f);
        gameObject.SetActive(false);
    }
    void ShowBloodEffect(Collider coll)
    {
        GameObject PBlood;
        Vector3 PBloodStart = new Vector3(coll.transform.position.x, coll.transform.position.y + 1.2f, coll.transform.position.z);
        float angle = Mathf.Atan2(coll.transform.position.x, coll.transform.position.z) * Mathf.Rad2Deg;
        PBlood = objectPoolManager.Spawn("PlayerBlood", PBloodStart, Quaternion.Euler(0, angle + coll.transform.rotation.y, 0));
        objectPoolManager.Despawn(PBlood, 20f);
    }
}
