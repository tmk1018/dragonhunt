﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EMeleeAttackRight : EState<EnemyController>
{
    public void OnEnter(EnemyController enemy)
    {
        enemy.animator.SetBool("MeleeAttackRight", true);
    }

    public void OnExit(EnemyController enemy)
    {
        enemy.animator.SetBool("MeleeAttackRight", false);
    }

    public void OnFixedUpdate(EnemyController enemy)
    {

    }

    public void OnUpdate(EnemyController enemy)
    {

    }
}
