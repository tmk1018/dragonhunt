﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ETracking : EState<EnemyController>
{
    public void OnEnter(EnemyController enemy)
    {
        Debug.Log("ETracking");
        enemy.animator.SetBool("Tracking", true);
    }

    public void OnExit(EnemyController enemy)
    {
        enemy.animator.SetBool("Tracking", false);
    }

    public void OnFixedUpdate(EnemyController enemy)
    {

    }

    public void OnUpdate(EnemyController enemy)
    {
        enemy.enemyBehavior.LookAtPlayerForTracking();
        float dist = enemy.enemyBehavior.distanceFromPlayer;
        
        if (dist <= enemy.enemyBehavior.attackDistance * enemy.enemyBehavior.attackDistance)
            enemy.ChangeState(EnemyController.EnemyState.EnemyAI);
    }
}
