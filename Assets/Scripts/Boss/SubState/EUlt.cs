﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EUlt : EState<EnemyController>
{
    public void OnEnter(EnemyController enemy)
    {
        enemy.animator.SetTrigger("EMateor");
    }

    public void OnExit(EnemyController enemy)
    {
        enemy.enemyBehavior.EMateorEnd();
    }

    public void OnFixedUpdate(EnemyController enemy)
    {

    }

    public void OnUpdate(EnemyController enemy)
    {


    }
}
