﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EFireBreath : EState<EnemyController>
{
    public void OnEnter(EnemyController enemy)
    {
        enemy.animator.SetBool("FireBreath", true);
        enemy.enemyPattern.WaitSec(1.8f);
        enemy.enemyBehavior.FireBreath();
    }

    public void OnExit(EnemyController enemy)
    {
        enemy.animator.SetBool("FireBreath", false);
    }

    public void OnFixedUpdate(EnemyController enemy)
    {

    }

    public void OnUpdate(EnemyController enemy)
    {

    }


}
