﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : EState<EnemyController>
{
    public void OnEnter(EnemyController enemy)
    {
        Debug.Log("EnemyAI");
    }

    public void OnExit(EnemyController enemy)
    {

    }

    public void OnFixedUpdate(EnemyController enemy)
    {
    }

    public void OnUpdate(EnemyController enemy)
    {
        float dist = enemy.enemyBehavior.distanceFromPlayer;
        float attackDistance = enemy.enemyBehavior.attackDistance;
        float sightDistance = enemy.enemyBehavior.sightDistance;

        if (dist <= attackDistance * attackDistance)
        {
            enemy.ChangeState(EnemyController.EnemyState.EAttackAI);
        }
        else if (dist > attackDistance * attackDistance)
        {
            enemy.ChangeState(EnemyController.EnemyState.ETracking);
        }
        else if (dist > sightDistance * sightDistance)
        {
            enemy.ChangeState(EnemyController.EnemyState.EIdle);
        }
    }
}
