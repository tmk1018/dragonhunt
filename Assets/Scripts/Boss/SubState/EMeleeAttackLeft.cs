﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EMeleeAttackLeft : EState<EnemyController>
{
    public void OnEnter(EnemyController enemy)
    {
        enemy.animator.SetBool("MeleeAttackLeft", true);
    }

    public void OnExit(EnemyController enemy)
    {
        enemy.animator.SetBool("MeleeAttackLeft", false);
    }

    public void OnFixedUpdate(EnemyController enemy)
    {

    }

    public void OnUpdate(EnemyController enemy)
    {
    }
}
