﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ETargetingPlayer : EState<EnemyController>
{
    public void OnEnter(EnemyController enemy)
    {
    }

    public void OnExit(EnemyController enemy)
    {

    }

    public void OnFixedUpdate(EnemyController enemy)
    {

    }

    public void OnUpdate(EnemyController enemy)
    {
        enemy.enemyBehavior.LookAtPlayerForAttack();
    }
}
