﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ESpitFire : EState<EnemyController>
{
    public void OnEnter(EnemyController enemy)
    {
        enemy.animator.SetBool("ESpitFire", true);
    }

    public void OnExit(EnemyController enemy)
    {
        enemy.animator.SetBool("ESpitFire", false);
    }

    public void OnFixedUpdate(EnemyController enemy)
    {

    }

    public void OnUpdate(EnemyController enemy)
    {

    }


}
