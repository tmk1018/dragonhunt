﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EIdle : EState<EnemyController>
{
    public void OnEnter(EnemyController enemy)
    {
        Debug.Log("EIdle");
    }

    public void OnExit(EnemyController enemy)
    {
    }

    public void OnFixedUpdate(EnemyController enemy)
    {

    }

    public void OnUpdate(EnemyController enemy)
    {
        if (enemy.enemyBehavior.deadCheck == false)
        {
            float dist = enemy.enemyBehavior.distanceFromPlayer;
            float sightDistance = enemy.enemyBehavior.sightDistance;


            if (dist <= sightDistance * sightDistance)
                enemy.ChangeState(EnemyController.EnemyState.ETracking);
        }
    }
}
