﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EFlying : EState<EnemyController>
{
    public void OnEnter(EnemyController enemy)
    {
        enemy.animator.SetBool("EnemyUlt", true);
    }

    public void OnExit(EnemyController enemy)
    {

    }

    public void OnFixedUpdate(EnemyController enemy)
    {

    }

    public void OnUpdate(EnemyController enemy)
    {
        enemy.enemyBehavior.LookAtPlayerForAttack();
        enemy.enemyBehavior.EFlyingToUltpos();
    }
}
