﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EAttackAI : EState<EnemyController>
{
    private bool ultCheck = false;

    public void OnEnter(EnemyController enemy)
    {
        Debug.Log("EAttackAI");

        int AiSelect = Random.Range(0, enemy.enemyPattern.enemyPatternCnt);
        Debug.Log(AiSelect);
        if (ultCheck == false && enemy.enemyBehavior.enemyTakeDamage.EUltCheck()==true)
        {
            AiSelect = 5;
            ultCheck = true;
        }
        switch (AiSelect)
        {
            case 0:
                enemy.StartCoroutine(enemy.enemyPattern.patternA());
                break;
            case 1:
                enemy.StartCoroutine(enemy.enemyPattern.patternB());
                break;
            case 2:
                enemy.StartCoroutine(enemy.enemyPattern.patternC());
                break;
            case 3:
                enemy.StartCoroutine(enemy.enemyPattern.patternD());
                break;
            case 4:
                enemy.StartCoroutine(enemy.enemyPattern.patternE());
                break;
            case 5:
                enemy.StartCoroutine(enemy.enemyPattern.patternUlt());
                break;
            default:
                Debug.LogError("맞는 패턴값이 아닙니다.");
                break;
        }
    }

    public void OnExit(EnemyController enemy)
    {

    }

    public void OnFixedUpdate(EnemyController enemy)
    {

    }

    public void OnUpdate(EnemyController enemy)
    {

    }
}
