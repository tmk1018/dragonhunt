﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ELanding : EState<EnemyController>
{
    public void OnEnter(EnemyController enemy)
    {

    }

    public void OnExit(EnemyController enemy)
    {
        enemy.animator.SetBool("EnemyUlt", false);
    }

    public void OnFixedUpdate(EnemyController enemy)
    {

    }

    public void OnUpdate(EnemyController enemy)
    {
        enemy.enemyBehavior.EFlyingToLand();
        enemy.enemyBehavior.LookAtPlayerForAttack();
        if (enemy.enemyBehavior.ELandingCheck() == true)
            enemy.ChangeState(EnemyController.EnemyState.ETracking);

    }
}
