﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EDead : EState<EnemyController>
{
    public void OnEnter(EnemyController enemy)
    {
        enemy.animator.SetTrigger("Death");
    }

    public void OnExit(EnemyController enemy)
    {
    }

    public void OnFixedUpdate(EnemyController enemy)
    {

    }

    public void OnUpdate(EnemyController enemy)
    {


    }
}
