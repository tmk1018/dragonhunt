﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBreathCollisionSpawn : MonoBehaviour
{
    [SerializeField]
    private ObjectPoolManager objectPoolManager;
    [SerializeField]
    private float spawnTime;
    private float cTimeCheck;

    void Update()
    {
        if (Time.time > cTimeCheck)
        {
            SpawnBreathCollision();

            cTimeCheck = Time.time + spawnTime;

        }
    }
    void SpawnBreathCollision()
    {
        GameObject EBreathCollision;
        EBreathCollision = objectPoolManager.Spawn("EBreathAttackCollision",
                this.transform.position, this.transform.rotation);
        objectPoolManager.Despawn(EBreathCollision, 5f);
    }
}