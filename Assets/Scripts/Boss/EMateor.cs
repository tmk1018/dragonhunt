﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EMateor: MonoBehaviour
{
    [SerializeField]
    private float MateorDmg;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerTakeDamage>().PTakeDamageWithSound(MateorDmg);
            this.gameObject.SetActive(false);
        }
    }
}
