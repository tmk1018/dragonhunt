﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EMateorRange : MonoBehaviour
{
    // 위에서 언급한 Plane의 자식인 RespawnRange 오브젝트
    public GameObject rangeObject;
    public GameObject eMateor;
    public GameObject eMagicCircle;
    BoxCollider rangeCollider;


    private void Awake()
    {
        rangeCollider = rangeObject.GetComponent<BoxCollider>();
    }
    private void Start()
    {
        StartCoroutine(RandomRespawn_Coroutine());
        StartCoroutine(PlayerTarget_Coroutine());
    }

    public Vector3 Return_RandomPosition()
    {
        Vector3 originPosition = rangeObject.transform.position;
        
        float range_X = rangeCollider.bounds.size.x;
        float range_Z = rangeCollider.bounds.size.z;

        range_X = Random.Range((range_X / 2) * -1, range_X / 2);
        range_Z = Random.Range((range_Z / 2) * -1, range_Z / 2);
        Vector3 RandomPostion = new Vector3(range_X, 0f, range_Z);

        Vector3 respawnPosition = originPosition + RandomPostion;
        return respawnPosition;
    }


    IEnumerator RandomRespawn_Coroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.2f);
            Vector3 MateorPosition = Return_RandomPosition();
            GameObject instantEMagicCircle = Instantiate(eMagicCircle, MateorPosition, Quaternion.Euler(90f, 0f, 0f));
            GameObject instantEMateor = Instantiate(eMateor, MateorPosition, Quaternion.identity);
            
        }
    }
    IEnumerator PlayerTarget_Coroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(1.5f);
            Vector3 MateorPosition = this.transform.position;
            GameObject instantEMagicCircle = Instantiate(eMagicCircle, MateorPosition, Quaternion.Euler(90f, 0f, 0f));
            GameObject instantEMateor = Instantiate(eMateor, MateorPosition, Quaternion.identity);

        }
    }
}
