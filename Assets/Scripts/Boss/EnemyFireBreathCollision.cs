﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFireBreathCollision : MonoBehaviour
{
    [SerializeField]
    private float breathCollisionSpeed;
    [SerializeField]
    private float breathDmg;
    private void Update()
    {
        transform.Translate(Vector3.forward * breathCollisionSpeed * Time.deltaTime);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerTakeDamage>().PTakeDamage(breathDmg);
            this.gameObject.SetActive(false);
        }
    }
}
