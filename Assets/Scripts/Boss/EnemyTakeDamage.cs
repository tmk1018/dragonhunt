﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTakeDamage : MonoBehaviour
{
    public float currhp = 1000.0f;
    public float hp = 1000f;

    private BoxCollider boxCollider;
    [SerializeField]
    private ObjectPoolManager objectPoolManager;
    [SerializeField]
    private float playerDamage;
    [SerializeField]
    private PlayerMovement playerMovement;
    private EnemyController enemyController;
    private EnemyBehavior enemyBehavior;
    [SerializeField]
    private EnemySoundFX enemySoundFX;
    [SerializeField]
    private CameraShake cameraShake;
    [SerializeField]
    private EnemyPattern enemypattern;
    void Start()
    {
        boxCollider = GetComponentInChildren<BoxCollider>();
        enemyController = GetComponentInParent<EnemyController>();
        enemyBehavior = GetComponentInParent<EnemyBehavior>();
    }

    private void Update()
    {
        if (!enemyBehavior.deadCheck)
            EnemyDead();
    }
    private void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "PlayerWeapon" && playerMovement.attackDelay == true)
        {
            playerMovement.attackDelay = false;
            ETakeDamageWithSound(playerDamage);
            ShowBloodEffect(coll);
            StartCoroutine(cameraShake.ShakeCamera(0.07f, 0.1f, 0.25f));
            Debug.Log("hit");
        }
    }
    void ShowBloodEffect(Collider coll)
    {
        GameObject EBlood;
        float angle = Mathf.Atan2(coll.transform.position.x, coll.transform.position.z) *
                      Mathf.Rad2Deg + 180;
        EBlood = objectPoolManager.Spawn("EnemyBlood", coll.transform.position,
                    Quaternion.Euler(0, angle + 90, 0));
        objectPoolManager.Despawn(EBlood, 5f);
    }
    void EnemyDead()
    {
        if (currhp <= 0)
        {
            enemyController.ChangeState(EnemyController.EnemyState.EDead);
            StopAllCoroutines();
            enemypattern.stopCoroutine();
        }
    }

    public void ETakeDamage(float damage)
    {
        currhp -= damage;
        Debug.Log("ETakeDamage");
    }
    public void ETakeDamageWithSound(float damage)
    {
        int ranInt = Random.Range(5, 10);
        currhp -= damage;
        enemySoundFX.audioSource.PlayOneShot(enemySoundFX.clips[ranInt]);
    }

    public bool EUltCheck()
    {
        if (hp * 0.3 > currhp) return true;
        else return false;
    }

    public void CameraShake()
    {
        StartCoroutine(cameraShake.ShakeCamera());
    }
}
