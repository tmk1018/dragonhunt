﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public enum EnemyState
    {
        EIdle,
        ETracking,
        EDead,

        EMeleeAttackLeft,
        EMeleeAttackRight,
        EFireBreath,
        EFlyingBreath,
        ETargetingPlayer,

        EFlying,
        EUlt,
        ELanding,

        EnemyAI,
        EAttackAI,
        ESpitFire


    }

    public Animator animator;//애니메이션 지정
    private EnemyStateMachine<EnemyController> e_sm;
    //스테이트들을 보관할 딕셔너리 설정
    private Dictionary<EnemyState, EState<EnemyController>> e_states = new Dictionary<EnemyState, EState<EnemyController>>();
    

    public EnemyPattern enemyPattern; //패턴 스크립트
    public EnemyBehavior enemyBehavior; //행동관련 스크립트

    private void Awake()
    {
    }

    void Start()
    {
        e_states.Add(EnemyState.EIdle, new EIdle());
        e_states.Add(EnemyState.EnemyAI, new EnemyAI());
        e_states.Add(EnemyState.ETracking, new ETracking());
        e_states.Add(EnemyState.EAttackAI, new EAttackAI());
        e_states.Add(EnemyState.ETargetingPlayer, new ETargetingPlayer());
        e_states.Add(EnemyState.EMeleeAttackLeft, new EMeleeAttackLeft());
        e_states.Add(EnemyState.EMeleeAttackRight, new EMeleeAttackRight());
        e_states.Add(EnemyState.EFireBreath, new EFireBreath());
        e_states.Add(EnemyState.ESpitFire, new ESpitFire());
        e_states.Add(EnemyState.EDead, new EDead());
        e_states.Add(EnemyState.EFlying, new EFlying());
        e_states.Add(EnemyState.EUlt, new EUlt());
        e_states.Add(EnemyState.ELanding, new ELanding());


        /*
        e_states.Add(EnemyState.EFlying, new EFlying());
        e_states.Add(EnemyState.EFlyingBreath, new EFlyingBreath());
        */

        animator = GetComponent<Animator>();
        enemyPattern = GetComponent<EnemyPattern>();
        enemyBehavior = GetComponent<EnemyBehavior>();
        e_sm = new EnemyStateMachine<EnemyController>(this, e_states[EnemyState.EIdle]);
    }

    public void ChangeState(EnemyState state)
    {
        Debug.LogWarning(state);
        e_sm.SetState(e_states[state]);
    }

    private void Update()
    {
        e_sm.OnUpdate();
    }

    private void FixedUpdate()
    {
        e_sm.OnFixedUpdate();
    }


  
}

