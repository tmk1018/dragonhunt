﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStateMachine<T>
{
    public EState<T> CurState { get; protected set; }

    private T m_sender;

    public EnemyStateMachine(T sender, EState<T> state)
    {
        m_sender = sender;
        SetState(state);
    }

    public void SetState(EState<T> state)
    {
        if (m_sender == null)
        {
            Debug.LogError("invalid m_sender");
            return;
        }

        if (CurState == state)
        {
            Debug.LogWarningFormat("Already Define State - {0}", state);
            return;
        }

        if (CurState != null)
            CurState.OnExit(m_sender);

        CurState = state;

        if (CurState != null)
            CurState.OnEnter(m_sender);
    }

    public void OnFixedUpdate()
    {
        if (m_sender == null)
        {
            Debug.LogError("invalid m_sener");
            return;
        }
        CurState.OnFixedUpdate(m_sender);
    }

    public void OnUpdate()
    {
        if (m_sender == null)
        {
            Debug.LogError("invalid m_sener");
            return;
        }
        CurState.OnUpdate(m_sender);
    }
}